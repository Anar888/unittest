package mynewproject.myproject843.service;

import static org.junit.jupiter.api.Assertions.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import mynewproject.myproject843.dto.AccountRequestDto;
import mynewproject.myproject843.dto.AccountResponseDto;
import mynewproject.myproject843.model.Account;
import mynewproject.myproject843.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;

    private Account mockAccount;


    @BeforeEach
    void setUp() {
        mockAccount = Account.builder()
                .id(15L)
                .name("Araz")
                .balance(560.0)
                .build();
    }


    @Test
    void givenValidIdWhenGetAccountThenSuccess() {
        //Arrange
        long id = 15L;
        when(accountRepository.findById(anyLong())).thenReturn(Optional.of(mockAccount));


        //Act
        Account account = accountService.getById(id);

        //Assert
        assertThat(account.getId()).isEqualTo(id);
        assertThat(account.getName()).isEqualTo("Araz");
        assertThat(account.getBalance()).isEqualTo(560.0);
        verify(accountRepository, times(1)).findById(id);
    }

    @Test
    void givenInvalidIdWhenGetAccountThenNotFound() {
        //Arrange
        long id = 15L;
        when(accountRepository.findById(id)).thenReturn(Optional.empty());

        //Act
        assertThatThrownBy(() -> accountService.getById(id))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Account not found");
    }

    @Test
    void givenValidDtoWhenCreateAccountThenSuccess() {
        //Arrange
        AccountRequestDto accountRequestDto = AccountRequestDto.builder()
                .name("Anar")
                .balance(123.2)
                .build();
        AccountResponseDto accountResponseDto = AccountResponseDto.builder()
                .name("Anar")
                .balance(123.2)
                .build();

        Account accountFromDb = Account.builder()
                .id(123L)
                .name("Anar")
                .balance(123.2)
                .build();
        when(accountRepository.save(any())).thenReturn(accountFromDb);

        //Act
        AccountResponseDto response = accountService.create(accountRequestDto);

        //Assert
        assertThat(response.getName()).isEqualTo(accountRequestDto.getName());
        assertThat(response.getBalance()).isEqualTo(accountRequestDto.getBalance());
        assertThat(response.getId()).isEqualTo(accountFromDb.getId());

        var captor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository, times(1)).save(captor.capture());

        assertThat(captor.getValue().getId()).isNull();
        assertThat(captor.getValue().getName()).isEqualTo("Anar");
        assertThat(captor.getValue().getBalance()).isEqualTo(123.2);
    }
    @Test
    void givenValidAccountUpdateThenSuccess() {
        Account account = Account.builder()
                .name("Anar")
                .balance(100.0)
                .build();

        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));



        Account updatedAccount = accountService.update(account);

        var captor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository, times(1)).save(captor.capture());


        assertThat(updatedAccount.getName()).isEqualTo("Anar");
        assertThat(updatedAccount.getBalance()).isEqualTo(100.0);


    }
    @Test
    void givenValidAccountUpdateThenNotFound(){

        // Create a new account object
        Account account = Account.builder()
                .id(200l)
                .name("Anar")
                .balance(100.0)
                .build();

        when(accountRepository.findById(account.getId())).thenReturn(Optional.empty());


        assertThatThrownBy(() -> accountService.getById(200l))
                .isInstanceOf(RuntimeException.class);


    }

    @Test
    void givenValidAccountDeleteThenSuccess(){
        Account account = Account.builder()
                .id(123L)
                .name("Anar")
                .balance(123.2)
                .build();
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        accountService.delete(account.getId());
        verify(accountRepository).delete(account);

}
    @Test
    void givenValidAccountDeleteThenNotFound(){
        when(accountRepository.findById(200L)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> accountService.getById(200L))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void givenValidAccountListThenSuccess(){
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(1L, "Ramin", 100.00));
        accounts.add(new Account(2L, "Tural", 200.00));

        when(accountRepository.findAll()).thenReturn(accounts);


        List<Account> result = accountService.findAll();

        assertThat(accounts)
                .isEqualTo(result);
    }

}

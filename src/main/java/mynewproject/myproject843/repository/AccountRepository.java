package mynewproject.myproject843.repository;

import mynewproject.myproject843.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long>{
}

package mynewproject.myproject843.service;


import lombok.RequiredArgsConstructor;
import mynewproject.myproject843.dto.AccountRequestDto;
import mynewproject.myproject843.dto.AccountResponseDto;
import mynewproject.myproject843.model.Account;
import mynewproject.myproject843.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public Account getById(Long id) {
        return accountRepository.findById(id).orElseThrow(() -> new RuntimeException("Account not found"));
    }

    public AccountResponseDto create(AccountRequestDto dto) {
        Account account = Account.builder()
                .name(dto.getName())
                .balance(dto.getBalance())
                .build();
        account = accountRepository.save(account);
        return AccountResponseDto.builder()
                .id(account.getId())
                .name(account.getName())
                .balance(account.getBalance())
                .build();
    }

    public Account update(Account account) {
        Account account1 = accountRepository.findById(account.getId()).orElseThrow(() -> new RuntimeException("Account not found"));
        account1.setName(account.getName());
        account1.setBalance(account.getBalance());
        accountRepository.save(account1);
        return account1;
    }

        public Account delete(Long id) {
        Account account1 = accountRepository.findById(id).orElseThrow(() -> new RuntimeException("Account not found"));

        accountRepository.delete(account1);
        return account1;
    }
    public List<Account> findAll(){
    List<Account> accounts=accountRepository.findAll();
    return accounts;
    }


}
